package com.tradevan.run;

import java.io.IOException;

import com.tradevan.operate.TestAUfoodstandards;
import com.tradevan.operate.TestCAinspection;
import com.tradevan.operate.TestHKcfs;
import com.tradevan.operate.XSoupExample;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class RunTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		 TestAUfoodstandards test = new TestAUfoodstandards();
		 TestHKcfs test = new TestHKcfs();
//		TestCAinspection test = new TestCAinspection();
		
		
		try {
			test.run();
			
			PropertyConfigurator.configure("src/main/resource/log4j.properties");
			Logger logger = Logger.getLogger(RunTest.class);
			logger.info("Hello Log4j, this is debug message");
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
