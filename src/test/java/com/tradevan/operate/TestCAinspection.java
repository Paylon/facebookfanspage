package com.tradevan.operate;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

public class TestCAinspection {

	public void run() throws IOException {
		// this.announcement("http://inspection.gc.ca/about-the-cfia/newsroom/food-recall-warnings/eng/1299076382077/1299076493846");
		this.detail("http://inspection.gc.ca/about-the-cfia/newsroom/food-recall-warnings/complete-listing/2017-05-12/eng/1494640129428/1494640131999");
	}

	private void announcement(String url) throws IOException {

		String domain = "http://inspection.gc.ca";
		System.out.println(String.format("Fetching %s", url));

		Document doc = Jsoup.connect(url).get();
		Elements es = null;
		Element e;

		List<String> trs = Xsoup
				.compile(
						"//table[@class='table table-striped table-hover']/tbody/tr")
				.evaluate(doc).list();

		for (String tr : trs) {
			es = Xsoup
					.compile("//td")
					.evaluate(
							Jsoup.parse("<table><tbody>" + tr
									+ "</tbody></table>")).getElements();
			for (int i = 0; i < es.size(); i++) {
				switch (i) {
				case 0: // DATE
					System.out.println(String.format(" Date:   (%s)", es.get(i)
							.text()));
					break;
				case 1: // RECALL & LINK
					System.out.println(String.format(" RECALL:   (%s)",
							es.get(i).text()));

					e = Xsoup.compile("//a")
							.evaluate(Jsoup.parse(es.get(i).html()))
							.getElements().first();
					System.out.println(String.format(" LINK:   (%s)", domain
							+ e.attr("href")));

					break;
				case 2: // CLASS
					System.out.println(String.format(" CLASS:   (%s)", es
							.get(i).text()));
					break;
				case 3: // DISTRIBUTION
					System.out.println(String.format(" DISTRIBUTION:   (%s)",
							es.get(i).text()));
					break;
				}
			}

		}

	}

	private void detail(String url) throws IOException {
		System.out.println(String.format("Fetching %s", url));

		Document doc = Jsoup.connect(url).get();

		Elements es, tds;
		Element e;
		// ===BLOCK 1===START===
		e = Xsoup.compile("//h1[@id='wb-cont']").evaluate(doc).getElements()
				.first();
		System.out.println(String.format(" RECALL:   (%s)", e.text())); // RECALL

		es = Xsoup.compile("//dl[@class='dl-horizontal']/dd").evaluate(doc)
				.getElements();
		for (int i = 0; i < es.size(); i++) {
			switch (i) {
			case 0:
				System.out.println(String.format(" DATE:   (%s)",
						this.getText(es.get(i)))); // DATE
				break;
			case 1:
				System.out.println(String.format(" REASON:   (%s)",
						this.getText(es.get(i)))); // REASON
				break;
			case 2:
				System.out.println(String.format(" CLASS:   (%s)",
						this.getText(es.get(i)))); // CLASS
				break;
			case 3:
				System.out.println(String.format(" COMPANY:   (%s)",
						this.getText(es.get(i)))); // COMPANY
				break;
			case 4:
				System.out.println(String.format(" DISTRIBUTION:   (%s)",
						this.getText(es.get(i)))); // DISTRIBUTION
				break;
			case 5:
				System.out.println(String.format(" EXTENT:   (%s)",
						this.getText(es.get(i)))); // EXTENT
				break;
			case 6:
				System.out.println(String.format(" REFERENCE:   (%s)",
						this.getText(es.get(i)))); // REFERENCE
				break;
			}
		}
		// ===BLOCK 1===END===

		// ===DETAIL===START===
		String detail = "", html = "";
		es = Xsoup.compile("//h2[@id='r01']").evaluate(doc).getElements();
		if (es.size() > 0) {
			es = es.first().parent().select("p"); // DETAIL
			detail = es.text().trim();
			System.out.println(String.format(" DETAIL:   (%s)", detail));
		}
		es = Xsoup.compile("//h2[@id='r01']").evaluate(doc).getElements();
		if (es.size() > 0) {
			html = es.first().parent().select("table").html(); // DETAIL INFOs

			es = Xsoup.compile("//table/tbody/tr")
					.evaluate(Jsoup.parse("<table>" + html + "</table>"))
					.getElements();
		} else {
			es.clear();
		}
		for (Element tr : es) {
			tds = tr.children();
			for (int i = 0; i < tds.size(); i++) {
				switch (i) {
				case 0:
					System.out.println(String.format(" STORE NAME:   (%s)",
							this.getText(tds.get(i))));
					break;
				case 1:
					System.out.println(String.format(" ADDRESS:   (%s)",
							this.getText(tds.get(i))));
					break;
				case 2:
					System.out.println(String.format(" PURCHASE DATES:   (%s)",
							this.getText(tds.get(i))));
					break;

				}
			}
		}
		// ===DETAIL===END===

		// ===PRODUCTS===START===
		es = Xsoup.compile("//h2[@id='r02']").evaluate(doc).getElements();
		if (es.size() > 0) {
			html = es.first().parent().select("table").html(); // PRODUCTS HTML
			es = Xsoup.compile("//table/tbody/tr")
					.evaluate(Jsoup.parse("<table>" + html + "</table>"))
					.getElements();
		} else {
			es.clear();
		}

		for (Element tr : es) {
			tds = tr.children();
			for (int i = 0; i < tds.size(); i++) {
				switch (i) {
				case 0:
					System.out.println(String.format(" BRAND NAME:   (%s)",
							this.getText(tds.get(i))));
					break;
				case 1:
					System.out.println(String.format(" COMMON NAME:   (%s)",
							this.getText(tds.get(i))));
					break;
				case 2:
					System.out.println(String.format(" SIZE:   (%s)",
							this.getText(tds.get(i))));
					break;
				case 3:
					System.out.println(String.format(
							" CODE ON PRODUCT:   (%s)",
							this.getText(tds.get(i))));
					break;
				case 4:
					System.out.println(String.format(" UPC:   (%s)",
							this.getText(tds.get(i))));
					break;
				}
			}
		}
		// ===PRODUCTS===END===

		// ===IMG===START===

		es = Xsoup.compile("//h2[@id='r08']").evaluate(doc).getElements();
		if (es.size() > 0) {
			es.first().parent().select("img"); // IMGs
		} else {
			es.clear();
		}

		for (Element img : es) {
			System.out.println(String.format(" IMG:   (%s)",
					img.attr("abs:src")));
		}
		// ===IMG===END===

	}

	private String getText(Element e) {
		return e.text().trim();
	}
}
