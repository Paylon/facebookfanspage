package com.tradevan.dbaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.tradevan.operate.AccessDatabase;
import com.tradevan.util.CeawlerConfig;
import com.tradevan.util.CrawlerConstant;

public class GetData {
	private static Logger logger = Logger.getLogger(GetData.class);

	public GetData() {
	}
	
	public List<String> getAllFansPageId() {
		String sql = "select fanspageid from committee_fanspage where fanspageid is not null";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		List<String> fansPageIdList = new ArrayList<String>();
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				String fanspageid = rs.getString("fanspageid");
				fansPageIdList.add(fanspageid);
			}
		} catch (Exception e) {
			logger.error("getAllFansPageId occur errors.", e);
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		return fansPageIdList;
	}

	public ResultSet getDataBySQL(String sql) {
		PreparedStatement ps = null;
		ResultSet result = null;
		Connection con = null;
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			result = ps.executeQuery();
		} catch (Exception e) {
			logger.error("getDataBySQL occur errors.", e);
		}

		return result;

	}

	public List<String> getFanPage(String init, String table) {

		PreparedStatement ps = null;
		ResultSet result = null;
		List<String> ganpages = new ArrayList<String>();

		String sql = "select fanspage from " + table;
		if (init.equals("1")) {
			sql += " where \"init\"='1'";
		}
		Connection con = null;
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			result = ps.executeQuery();

			while (result.next()) {
				ganpages.add(result.getString(1));
			}

		} catch (Exception e) {
			logger.error("getDataBySQL occur errors.", e);
		}
		return ganpages;

	}

	public void execute(String sql) {
		Statement ps = null;
		Connection con = null;
		try {
			con = AccessDatabase.getConnection();
			ps = con.createStatement();
			ps.execute(sql);
		} catch (Exception e) {
			logger.error("getDataBySQL occur errors.", e);
		}
	}
}
