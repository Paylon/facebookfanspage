package com.tradevan.bean;

public class ObjPlace {

  private String id, name, maincategory, categoryid, categoryname, like, checkins, wereHereCount, talkingAboutCount;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMaincategory() {
    return maincategory;
  }

  public void setMaincategory(String maincategory) {
    this.maincategory = maincategory;
  }

  public String getCategoryid() {
    return categoryid;
  }

  public void setCategoryid(String categoryid) {
    this.categoryid = categoryid;
  }

  public String getCategoryname() {
    return categoryname;
  }

  public void setCategoryname(String categoryname) {
    this.categoryname = categoryname;
  }

  public String getLike() {
    return like;
  }

  public void setLike(String like) {
    this.like = like;
  }

  public String getCheckins() {
    return checkins;
  }

  public void setCheckins(String checkins) {
    this.checkins = checkins;
  }

  public String getWereHereCount() {
    return wereHereCount;
  }

  public void setWereHereCount(String wereHereCount) {
    this.wereHereCount = wereHereCount;
  }

  public String getTalkingAboutCount() {
    return talkingAboutCount;
  }

  public void setTalkingAboutCount(String talkingAboutCount) {
    this.talkingAboutCount = talkingAboutCount;
  }

}
