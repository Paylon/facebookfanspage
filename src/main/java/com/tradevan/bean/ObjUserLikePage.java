package com.tradevan.bean;

public class ObjUserLikePage {

  private String userid, username, likePageId, likePageName, mainCategory, categoryId, categoryName;

  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getLikePageId() {
    return likePageId;
  }

  public void setLikePageId(String likePageId) {
    this.likePageId = likePageId;
  }

  public String getLikePageName() {
    return likePageName;
  }

  public void setLikePageName(String likePageName) {
    this.likePageName = likePageName;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getMainCategory() {
    return mainCategory;
  }

  public void setMainCategory(String mainCategory) {
    this.mainCategory = mainCategory;
  }

}
