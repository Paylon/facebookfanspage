package com.tradevan.bean;

public class ObjComments {

	private String pageId, postId, userId, userName, createdTime, messageTagId, messageTagName, message,
			shares, likes, comment_count;

	public String getComment_count() {
		return comment_count;
	}

	public void setComment_count(String comment_count) {
		this.comment_count = comment_count;
	}

	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}

	public String toString() {
		String text = "============";
		text += "\npageId: " + getPageId();
		text += "\npostId: " + getPostId();
		text += "\nuserId: " + getUserId();
		text += "\nuserName: " + getUserName();
		text += "\ncreatedTime: " + getCreatedTime();
		text += "\nmessageTagId: " + getMessageTagId();
		text += "\nmessageTagName: " + getMessageTagName();
		text += "\nmessage: " + getMessage();
		text += "\nshares: " + getShares();
		text += "\n=============";
		return text;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getShares() {
		return shares;
	}

	public void setShares(String shares) {
		this.shares = shares;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageTagId() {
		return messageTagId;
	}

	public void setMessageTagId(String messageTagId) {
		this.messageTagId = messageTagId;
	}

	public String getMessageTagName() {
		return messageTagName;
	}

	public void setMessageTagName(String messageTagName) {
		this.messageTagName = messageTagName;
	}

}
