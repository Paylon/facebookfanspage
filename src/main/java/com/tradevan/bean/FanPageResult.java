package com.tradevan.bean;

public class FanPageResult {

  private String pagename, pageid;

  private int number;

  public String getPagename() {
    return pagename;
  }

  public void setPagename(String pagename) {
    this.pagename = pagename;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  public String getPageid() {
    return pageid;
  }

  public void setPageid(String pageid) {
    this.pageid = pageid;
  }

}
