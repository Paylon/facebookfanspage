package com.tradevan.model;

public class ProductInfo {
	private String id, brandName, commonName, description, size, codeOnProduct, 
    item, caseUpc, itemUpc, packaging, cases, bestByDate ;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getCodeOnProduct() {
		return codeOnProduct;
	}

	public void setCodeOnProduct(String codeOnProduct) {
		this.codeOnProduct = codeOnProduct;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getCaseUpc() {
		return caseUpc;
	}

	public void setCaseUpc(String caseUpc) {
		this.caseUpc = caseUpc;
	}

	public String getItemUpc() {
		return itemUpc;
	}

	public void setItemUpc(String itemUpc) {
		this.itemUpc = itemUpc;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public String getCases() {
		return cases;
	}

	public void setCases(String cases) {
		this.cases = cases;
	}

	public String getBestByDate() {
		return bestByDate;
	}

	public void setBestByDate(String bestByDate) {
		this.bestByDate = bestByDate;
	}

	
}
