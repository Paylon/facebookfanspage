package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TableStatisticsProp {

	private String tablename = "", id = "", flag = "", website = "",
	successCount = "", failure = "", updatetime = "",
			timeOut = "", weberror = "", nullData = "";

	public TableStatisticsProp() {
		FileInputStream fis = null;
		try {
//			TEST
			fis = new FileInputStream(
					"src/main/resource/table_statistics.properties");
//			FORMAL
//			fis = new FileInputStream(
//			"C:/workspace/lib/src/main/resource/table_statistics.properties");			
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					if (read_line.toLowerCase().indexOf("tablename:") >= 0) {
						this.tablename = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("id:") >= 0) {
						this.id = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("flag:") >= 0) {
						this.flag = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("website:") >= 0) {
						this.website = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("successcount:") >= 0) {
						this.successCount = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("failure:") >= 0) {
						this.failure = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("updatetime:") >= 0) {
						this.updatetime = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("timeout:") >= 0) {
						this.timeOut = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("weberror:") >= 0) {
						this.weberror = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("nulldata:") >= 0) {
						this.nullData = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getTablename() {
		return tablename;
	}

	public String getId() {
		return id;
	}

	public String getFlag() {
		return flag;
	}

	public String getWebsite() {
		return website;
	}

	public String getSuccessCount() {
		return successCount;
	}

	public String getFailure() {
		return failure;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public String getWebError() {
		return weberror;
	}

	public String getNullData() {
		return nullData;
	}

	
	

}
